**Halium 10.0/UBPorts Progress on Redmi Note 9S**

**Works:**
- GUI
- Touch
- Sensors (Rotation, Proximity)
- Brightness
- Airplane Mode
- Wi-Fi
- Sound
- AppArmor
- Headphone Jack
- Bluetooth
- Libertine

**Partially Working:**
- RIL (everything works except mobile data)
- Camera (takes pics, wont record vid)

**Does not work:**
- Flashlight
- GPS
- Notification LED
- Vibration motor
- SD Card
- MTP
